import cv2
import numpy as np
values = open("thresh_values.txt","w")
setup_color = input("red, green, blue or yellow: ")
tValues = {'green':[29,65,135,248,213,171],
    'yellow':[24,102,117,28,255,255],
    'blue':[90,140,169,223,255,231],
    'red':[3,244,181,18,255,246]
    }

def updateValue(index,newValue):
     # make sure to write the new value into the global variable
     tValues[setup_color][index] = newValue
     return
cap = cv2.VideoCapture(0)
cv2.namedWindow("Trackbars")
cv2.createTrackbar("LH", "Trackbars", tValues[setup_color][0], 255, lambda newValue : updateValue(0,newValue))
cv2.createTrackbar("LS", "Trackbars", tValues[setup_color][1], 255, lambda newValue : updateValue(1,newValue))
cv2.createTrackbar("LV", "Trackbars", tValues[setup_color][2], 255, lambda newValue : updateValue(2,newValue))
cv2.createTrackbar("HH", "Trackbars", tValues[setup_color][3], 255, lambda newValue : updateValue(3,newValue))
cv2.createTrackbar("HS", "Trackbars", tValues[setup_color][4], 255, lambda newValue : updateValue(4,newValue))
cv2.createTrackbar("HV", "Trackbars", tValues[setup_color][5], 255, lambda newValue : updateValue(5,newValue))
img = cv2.imread('messi.jpg')
n = 5
cv2.imshow('Trackbars',img)
while True:
    ret, frame = cap.read()
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    # colour detection limits
    lH = 125
    lS = 125
    lV = 125
    hH = 255
    hS = 255
    hV = 255
    lowerLimits = np.array([tValues[setup_color][0], tValues[setup_color][1], tValues[setup_color][2]])
    upperLimits = np.array([tValues[setup_color][3], tValues[setup_color][4], tValues[setup_color][5]])
    #liiga nõrk
    blur = cv2.blur(frame,(n,n))

    thresholded = cv2.inRange(blur, lowerLimits, upperLimits)
    thresholded = cv2.bitwise_not(thresholded)
    cv2.imshow('Thresholded', thresholded)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
values.write(str(tValues))
values.close()
print("closing program")
cap.release()
cv2.destroyAllWindows()

