import numpy as np
import cv2
import time
from pyzbar import pyzbar
import argparse
tValues = [72,51,21,130,208,231]
def updateValue(index,newValue):
	# make sure to write the new value into the global variable
	tValues[index] = newValue
	return
cv2.namedWindow("Processed")
# open the camera
cap = cv2.VideoCapture(0)
cv2.createTrackbar("LH", "Processed", tValues[0], 255, lambda newValue : updateValue(0,newValue))
cv2.createTrackbar("LS", "Processed", tValues[1], 255, lambda newValue : updateValue(1,newValue))
cv2.createTrackbar("LV", "Processed", tValues[2], 255, lambda newValue : updateValue(2,newValue))
cv2.createTrackbar("HH", "Processed", tValues[3], 255, lambda newValue : updateValue(3,newValue))
cv2.createTrackbar("HS", "Processed", tValues[4], 255, lambda newValue : updateValue(4,newValue))
cv2.createTrackbar("HV", "Processed", tValues[5], 255, lambda newValue : updateValue(5,newValue))
n = 5
kernel = np.ones((n,n),np.uint8)

while True:
    ret, frame = cap.read()
    r = [len(frame)-180, len(frame)-150, 0, len(frame[0])]
    frame=frame[r[0]:r[1], r[2]:r[3]]

    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    lH = 125
    lS = 125
    lV = 125
    hH = 255
    hS = 255
    hV = 255
    lowerLimits = np.array([tValues[0], tValues[1], tValues[2]])
    upperLimits = np.array([tValues[3], tValues[4], tValues[5]])
    
    closing = cv2.morphologyEx(frame, cv2.MORPH_CLOSE, kernel)

    thresholded = cv2.inRange(closing, lowerLimits, upperLimits)
    thresholded = cv2.bitwise_not(thresholded)
    cv2.imshow('Thresholded', thresholded)
    outimage = cv2.bitwise_and(frame, frame, mask = thresholded)
    cv2.imshow('Original',outimage)
    
    #barcode stuff
    barcodes = pyzbar.decode(thresholded)
    if len(barcodes) > 0:
        barcodeData = barcodes[0].data.decode("utf-8")
        barcodeType = barcodes[0].type
        print("[INFO] Found {} barcode: {}".format(barcodeType, barcodeData))
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
print('closing program')
cap.release()
cv2.destroyAllWindows()
