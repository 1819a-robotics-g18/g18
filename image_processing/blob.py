# coding=utf-8
import numpy as np
import cv2
import ast
import time
import threading
from math import floor
from matplotlib import pyplot as plt
#setup_color = input('color pls')
#tValues = {'green':[29,65,135,248,213,171],
#        'yellow':[24,102,117,28,255,255],
#        'blue':[90,140,169,223,255,231],
#        'red':[3,244,181,18,255,246]
#        }
#def updateValue(index,newValue):
#    # make sure to write the new value into the global variable
#    tValues[setup_color][index] = newValue
#    return
# reading thresh values from a file
threshIn = open('thresh_values.txt','r')
tValues = threshIn.read()
tValues = ast.literal_eval(tValues)
def getDistanceWithCam(blobSize):
    if blobSize > 0:
        return 59915.85/blobSize - 367.47
    return -1

start = time.time()
# Open the camera
cap = cv2.VideoCapture(0)
#cv2.namedWindow("Trackbars")
#cv2.createTrackbar("LH", "Trackbars", tValues[setup_color][0], 255, lambda newValue : updateValue(0,newValue))
#cv2.createTrackbar("LS", "Trackbars", tValues[setup_color][1], 255, lambda newValue : updateValue(1,newValue))
#cv2.createTrackbar("LV", "Trackbars", tValues[setup_color][2], 255, lambda newValue : updateValue(2,newValue))
#cv2.createTrackbar("HH", "Trackbars", tValues[setup_color][3], 255, lambda newValue : updateValue(3,newValue))
#cv2.createTrackbar("HS", "Trackbars", tValues[setup_color][4], 255, lambda newValue : updateValue(4,newValue))
#cv2.createTrackbar("HV", "Trackbars", tValues[setup_color][5], 255, lambda newValue : updateValue(5,newValue))
blobparams = cv2.SimpleBlobDetector_Params()
blobparams.filterByArea = True
blobparams.minArea = 300
blobparams.filterByConvexity = False
blobparams.maxArea = 700000
blobparams.filterByCircularity = False
blobparams.minDistBetweenBlobs = 10
detector = cv2.SimpleBlobDetector_create(blobparams)

n = 5
kernel = np.ones((n,n),np.uint8)
#img = cv2.imread('messi.jpg')
#cv2.imshow('Trackbars',img)
def main(color):
    # Read the image from the camera
    ret, frame = cap.read()
    #You will need this later
#    r = [len(frame)-200,len(frame)-190,0,len(frame[0])]
#    frame = frame[r[0]:r[1],r[2]:r[3]]
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    # colour detection limits
    lH = 125
    lS = 125
    lV = 125
    hH = 255
    hS = 255
    hV = 255
    lowerLimits = np.array([tValues[color][0], tValues[color][1], tValues[color][2]])
    upperLimits = np.array([tValues[color][3], tValues[color][4], tValues[color][5]])
    #liiga nõrk
    blur = cv2.blur(frame,(n,n))

    thresholded = cv2.inRange(blur, lowerLimits, upperLimits)
    thresholded = cv2.bitwise_not(thresholded)
    cv2.imshow('Thresholded', thresholded)

    keypoints = detector.detect(thresholded)
    if len(keypoints) > 0:
        print(color)
        print(getDistanceWithCam(keypoints[0].size))
    # Quit the program when 'q' is pressed
#threads
while True:
    t1 = threading.Thread(target=main,args=('red',))
    t2 = threading.Thread(target=main,args=('green',))
    t3 = threading.Thread(target=main,args=('blue',))
    t4 = threading.Thread(target=main,args=('yellow',))
    t1.start()
    t2.start()
    t3.start()
    t4.start()
    t1.join()
    t2.join()
    t3.join()
    t4.join()

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
print("closing program")
threshIn.close()
cap.release()
cv2.destroyAllWindows()

