#**Robotics project**
#**Image trained and voice controlled circus robot**

**Team members:**

Janeli Takk  
Andri Ots  
Sten Salumets  
J�ri J�ul

## Voice control
### Task description

The part of voice controlled robot will introduce us to the world of speech
recognition. The aim of the sub-project is to develop a robot that can understand
voice instructions via a microphone and act accordingly using the Raspberry Pi and
the GoPiGo platforms. The commands include but are not limited to: driving forward
by a certain distance, turning by a certain number of degrees, stopping, setting
the speed of the robot etc. The final list of commands will be determined during
the development. If the workload seems manageable we will also complement the voice
recognition system to allow more freely uttered commands, so as not to make users 
memorize the whole list of possibilities. Also, a bonus goal of ours is to make the 
robot react to both English and Estonian languages.

Our task specific output is just a controller, which takes a voice sample as an input
and returns a command to be used by the robot. The command itself, whether it be moving
or stopping or something else, is then processed by the robot to determine the execution
of said command. This latter part requires a lot of teamwork between the pairs to
determine the exact mechanics of movement and processing commands.

The robot should also be able to drive around the room autonomously, using a variety
of sensors. The purpose of this is to navigate the room to find images that could make
it do actions implemented by the whole team. The search for images will otherwise only
be active when given the proper command orally, thus improving performance. Naturally,
image recognition itself will be done by the other pair.

In the end, both of the pairs will get together to implement the aforementioned autonomous
life system for the robot, which features exploring the room while also avoiding collisions
with other objects. And who knows, the robot may even voice its boredom when not getting
sufficient action over a period of time.

## Image processing
### Task description

As described above the GoPiGo robot will be roaming around the room with voice commands
and seeking for objects/patterns that will signal the robot to perform circus stunts.
For example if the robot might happen to find a golf ball and it turns out to be orange
then it performs of circular movement. If the robot detects a green golf ball it navigates
next to it. As Raspberry Pi has very limited processing power we might need to send the
camera feed to a more suitable remote computer signal the robot from there via WiFi if a
stunt is to be performed. This would require developing a communication with the Raspberry
Pi and a remote system.  The remote system would be most likely using a GNU/Linux operating
system such as Ubuntu. As for complexity if we could combine image processing and voice
recognition - for example if an orange golf ball is detected the robot could wait for a
voice command what should determine how many circles the robot should complete. This would
require the Raspberry Pi to have a speaker which prompts user for voice input.

## Work schedule for speech recognition

Week     |      Work done   |  Time spent
-------- | ---------------- | ------------
8th week | Creating a repository| 2h
9th week | Creating the project plan. Dividing the tasks between pair partners. First separate code developments. Agreements between pairs on how the modules communicate with each other | 4h
10th week | Creating the project plan. Dividing the tasks between pair partners. First separate code developments. | 5h
11th week | Further code developments. At this stage, the robot should be able to recognize the first vocal commands and print them out to the console. Slowly starting to combine the code of both partners. | 10h
12th week | Filming the demo video, recap on the challenges and solutions. Further code development. First implementations with a physical robot. Some commands will have been finished.| 20h
13th week | Presenting the demo video. Combining the features of both partners into one. Testing, evaluation of pros and cons, verdict. Fixing the appeared problems in pairs. | 20h
14th week | Designing the poster. Meetup with the whole group, testing. | 14h
15th week | Testing the finished robot in different circumstances. Fixing possible bugs in the code, fine finish. Poster session with the live demo. End of project | 23h

## Work schedule for image processing

Week     |      Work done   |  Time spent
-------- | ---------------- | ------------
8th week | Creating a repository| 2h
9th week | Gathering ideas and getting familiar with the necessary Python modules | 4h
10th week | Creating the project plan. Dividing the tasks between pair partners. First separate code developments. | 5h
11th week | Further code developments. Mainly getting WiFi communications working and deciding what to look for in the room. Slowly starting to combine the code of both partners. | 12h
12th week | Filming the demo video, recap on the challenges and solutions. Further code development. First implementations with a physical robot. Some commands will have been finished.| 20h
13th week | Presenting the demo video. Combining the features of both partners into one. Testing, evaluation of pros and cons, verdict. Fixing the appeared problems in pairs. | 20h
14th week | Designing the poster. Meetup with the whole group, testing. | 14h
15th week | Testing the finished robot in different circumstances. Fixing possible bugs in the code, fine finish. Poster session with the live demo. End of project | 23h


## List of necessary hardware

Item   | Quantity | Should it be provided for us?
------ | -------- | ------------------------------
Raspberry Pi | 2 | Yes
GoPiGo | 1 | Yes
Webcam | 1(+1) | Yes
Microphone (if the webcam doesn't have an integrated one) | 1 | Yes
Speaker, mountable on a GoPiGo robot | 1 | Yes
A laptop with fomidable processing power | 1 | No
Ultrasonic distance sensors | 2 | Yes
Non-continuous servo | 1 | Yes

## Challenges and Solutions
### Voice control
Challenges	|            | Solutions
------------|----------- | -------------------
Finding the most reliable voice recognition system | The best ones are usually costly/have a daily limit and the free ones are not completely reliable. | Daily limit is enough to manage the robot. After testing, it didn't disturb the workflow
Understanding and implementing different frameworks | Voice recognition systems have many libraries so it is difficult to adjust between them. | Google can open up the world to you
Reliable and fast keyword detection | Keyword detection does not always recognize the keywords and takes a bit of time also. | Make the robot play a sound, when keyword is detected. Increase microphone gain.
Longer and more complex commands | How to implement longer commands, so that the robot can understand it. Extracting the numerical values from phrases effectively | Using if-sentences (for example, if word "forward" in command, execute action).
Getting the robot to "speak" | Using speakers with Raspberry Pi proves to be difficult | Trying to test different libraries to find the best solution.