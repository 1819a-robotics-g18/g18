import socket
import cmd
from generaator.generaator import *
import subprocess
import time
import threading
import _thread
import gopigo as go
from gopigo import *
import time
import random
import serial
import json

go.stop()
time.sleep(2)
gospeed = 100
gospeed_väärtus = 100

muusika_mängib = 0

kontroll = 0
aeg = time.time()
eelmine_aeg = aeg
autonomous = 0
i = 0

ser = serial.Serial('/dev/ttyUSB0', 9600)

# Make sure arduino is ready to send the data.
print("Syncing serial...0%\r", end='')
while ser.in_waiting == 0:
    ser.write("R".encode())
print("Syncing serial...50%\r", end='')
while ser.in_waiting > 0:
    ser.readline()
print("Syncing serial...100%")

def autonomous():
    global kontroll
    global aeg
    global eelmine_aeg
    global muusika_mängib

    ajamuutuja = 15

    while True:
        # Measure the time passed
        aeg = time.time()

        if aeg - eelmine_aeg >= ajamuutuja:
            eelmine_aeg = aeg
            kontroll = 1
            print("Möödunud on", ajamuutuja, "sekundit..")
            print("Mul on igav ning jätkan autonoomselt!")
            if muusika_mängib == 0:
                muusika("Darude.mp3")
                muusika_mängib = 1


        # Autonomous logic

        if kontroll == 1:

            ser.write("R".encode())  # Send something to the Arduino to indicate we're ready to get some data.
            serial_line = ser.readline().strip()  # Read the sent data from serial.

            try:

                # Decode the received JSON data
                data = json.loads(serial_line.decode())
                # Extract the sensor value
                dist = data['us1']
                # dist2 = data['us2']
            except Exception as e:  # Something went wrong extracting the JSON.
                dist = -1  # Handle the situation.
                print(e)
                pass

            # We are moving

            if kontroll == 1:
                if dist != -1:
                    go.motor1(1, gospeed)
                    go.motor2(1, gospeed)
                    if dist < 900:
                        go.right_rot()


#ajalõim = threading.Thread(target=aeg)
#ajalõim.start()

print("Avan uue lõime..")
#lõim = threading.Thread(target=autonomous)
#lõim.start()
_thread.start_new_thread(autonomous, ())

server_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

try:
    PORT_NR = 7777

    server_socket.bind(('', PORT_NR))

    while True:
        go.stop()

        print("Kord:", i)
        dataFromClient, address = server_socket.recvfrom(256)

        # Decode the bitstream
        dataFromClient = dataFromClient.decode("utf8").strip()

        print(dataFromClient)

        # Call out the necessary commands

        # Set the autonomous control variable to 0, which in turn sends the kill command to the autonomous process.
        kontroll = 0
        muusika_mängib = 0
        go.stop()

        # Process the data
        if dataFromClient == "tere":
            print("tereteretere")

        if dataFromClient == "autonomous":
            protsess = subprocess.Popen(["python3", "auto.py"])
            print("Alustasin autonoomsust käsu järgi!")

        if dataFromClient == "enough":
            protsess.terminate()

        go.stop()
        kontroll = 0
        käsk = eval(dataFromClient)

        # Set the previous time as current time
        eelmine_aeg = time.time()
        go.stop()

        # that is, execute the given command in the cmd module

        i += 1

finally:
    go.stop()
    print("Head aega!")
    go.stop()
    server_socket.close()
