#import gopigo as go
import os
from pocketsphinx import pocketsphinx
from sphinxbase import sphinxbase
import pyaudio
import speech_recognition as sr
# from speech_recognition import *
#from generaator.generaator import *
#import cmd
from Commander.Commander import *

IP = "192.168.43.63"
PORT = 7777

commander = Commander(IP, PORT)

r = sr.Recognizer()
mic = sr.Microphone()
with mic as source:
    r.adjust_for_ambient_noise(source)


def start_keyphrase_recognition(keyphrase_function, key_phrase):
    """ Starts a thread that is always listening for a specific key phrase. Once the
        key phrase is recognized, the thread will call the keyphrase_function. This
        function is called within the thread (a new thread is not started), so the
        key phrase detection is paused until the function returns.

    :param keyphrase_function: function that is called when the phrase is recognized
    :param key_phrase: a string for the key phrase
    """
    modeldir = "files/sphinx/models"

    # Create a decoder with certain model
    config = pocketsphinx.Decoder.default_config()
    # Use the mobile voice model (en-us-ptm) for performance constrained systems
    config.set_string('-hmm', os.path.join(modeldir, 'en-us/en-us-ptm'))
    # config.set_string('-hmm', os.path.join(modeldir, 'en-us/en-us'))
    config.set_string('-dict', os.path.join(modeldir, 'en-us/cmudict-en-us.dict'))
    config.set_string('-keyphrase', key_phrase)
    config.set_string('-logfn', 'files/sphinx.log')
    config.set_float('-kws_threshold', 1e-15)

    # Start a pyaudio instance
    p = pyaudio.PyAudio()
    # Create an input stream with pyaudio
    stream = p.open(format=pyaudio.paInt16, channels=1, rate=16000, input=True, frames_per_buffer=1024)
    # Start the stream
    stream.start_stream()

    # Process audio chunk by chunk. On keyword detected perform action and restart search
    decoder = pocketsphinx.Decoder(config)
    decoder.start_utt()
    print("Listening")
    # Loop forever
    while True:
        # Read 1024 samples from the buffer
        buf = stream.read(1024, exception_on_overflow=False)
        # If data in the buffer, process using the sphinx decoder
        if buf:
            decoder.process_raw(buf, False, False)
        else:
            break
        # If the hypothesis is not none, the key phrase was recognized
        if decoder.hyp() is not None:
            stream.close()
            decoder.end_utt()
            keyphrase_function()
            # Stop and reinitialize the decoder
            stream = p.open(format=pyaudio.paInt16, channels=1, rate=16000, input=True, frames_per_buffer=1024)

            stream.start_stream()
            decoder.start_utt()


# Command detection
def command_detection():
    commander.sendCommand("muusika('Yah.mp3')")
    try:
        with mic as source:
            # Wait for commands...
            print("Talk...")
            audio = r.listen(source)
            print("Recognizing")
            käsklus = r.recognize_google(audio)
    except:
        print("Põrusin")
        käsklus = ""
    # Show the command in the console
    print("Käsklus: ", käsklus)
    
    # ACTIONS BASED ON VOCAL COMMANDS

    # The default parameter
    var = 1

    # Find a parameter if it exists
    käsklus_list = käsklus.split()
    for element in käsklus_list:
        if element.isnumeric():
            var = float(element)

    # Move commands:
    if "move" in käsklus or "go" in käsklus:

        käsklus_list = käsklus.split()
        for element in käsklus_list:
            if element.isnumeric():
                var = float(element)

        if "forward" in käsklus:
            commander.sendCommand("cmd.meetrit_edasi("+str(var)+")")
        elif "backward" in käsklus or "back" in käsklus:
            commander.sendCommand("cmd.meetrit_tagasi(" + str(var) + ")")

        elif "left" in käsklus:
            if var == 1:
                var = 90
            commander.sendCommand("cmd.left(" + str(var) + ")")

        elif "right" in käsklus:
            if var == 1:
                var = 90
            commander.sendCommand("cmd.right(" + str(var) + ")")

    if "change" in käsklus and "speed" in käsklus:

            commander.sendCommand("cmd.muuda_kiirust(" + str(var) + ")")

    if "rotate" in käsklus:
        commander.sendCommand("cmd.twirl()")

    if "cheated" in käsklus:
        commander.sendCommand("muusika('Hate.mp3')")
        commander.sendCommand("cmd.cheated()")
    if "sing" in käsklus:
        commander.sendCommand("muusika('Christmas.mp3')")
    
    # Stop command
    if "stop" in käsklus:
        if "singing" in käsklus:
            commander.sendCommand("stopp()")
        else:
            commander.sendCommand("cmd.peatu()")


    if "autonomous" in käsklus:
        commander.sendCommand("autonomous")

    if "enough" in käsklus:
        commander.sendCommand("enough")

    # Start key phrase recognition and call the command detection function when triggered

start_keyphrase_recognition(command_detection, "jenny")

commander.sendCommand("Q")
commander.closeSocket()