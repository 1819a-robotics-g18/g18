#!/usr/bin/env python
# -*- coding: utf-8 -*-

from Commander import *

IP = "192.168.43.172"
PORT = 7777

commander = Commander(IP, PORT)

try:
    while True:
        command = input("Sisesta käsk: ")

        if command.strip() == "Q":
            break

        commander.sendCommand(command)

finally:
    commander.closeSocket()
