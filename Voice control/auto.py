def autonomous():
    while True:

        ser.write("R".encode())  # Send something to the Arduino to indicate we're ready to get some data.
        serial_line = ser.readline().strip()  # Read the sent data from serial.

        try:

            # Decode the received JSON data
            data = json.loads(serial_line.decode())
            # Extract the sensor value
            dist = data['us1']
            # dist2 = data['us2']
        except Exception as e:  # Something went wrong extracting the JSON.
            dist = -1  # Handle the situation.
            print(e)
            pass

        if dist != -1:
            go.motor1(1, gospeed)
            go.motor2(1, gospeed)
            if dist < 900:
                go.right()