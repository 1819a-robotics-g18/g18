import gopigo as go
from gopigo import *
import time
import random
import serial
import json
from random import randint
import subprocess

gospeed = 200
gospeed_väärtus = 200

def meetrit_edasi(t):
    global gospeed_väärtus
    gospeed = gospeed_väärtus
    enc_start = go.enc_read(0)
    #üks enc väärtus on võrdne 0.01134 meetriga
    edasi_minna = t / 0.01134
    while True:
        go.motor1(1,gospeed)
        go.motor2(1,gospeed)
        if go.enc_read(0) - enc_start >= edasi_minna:
            go.stop()
            break
        
def meetrit_tagasi(t):
    global gospeed_väärtus
    gospeed = gospeed_väärtus
    enc_start = go.enc_read(0)
    #üks enc väärtus on võrdne 0.01134 meetriga
    tagasi_minna = t / 0.01134
    while True:
        #go.bwd()
        go.bwd()
        if go.enc_read(0) - enc_start >= tagasi_minna:
            go.stop()
            break
        
def left(t=90):
    global gospeed_väärtus
    gospeed = 120
    go.left()
    time.sleep(0.00722222222*t)
    go.stop()
    gospeed = gospeed_väärtus
    
def right(t=90):
    global gospeed_väärtus
    gospeed = 120
    go.right()
    time.sleep(0.00722222222*t)
    go.stop()
    gospeed = gospeed_väärtus
    
def muuda_kiirust(t):
    global gospeed
    global gospeed_väärtus
    gospeed = t
    gospeed_väärtus = t
    
def peatu():
    go.stop()
    
def vasak_kiirus(t):
    go.set_left_speed(t)
    
def parem_kiirus(t):
    go.set_right_speed(t)

def twirl():
    global gospeed_väärtus
    gospeed = 200
    go.right_rot()
    time.sleep(5)
    go.stop()
    gospeed = gospeed_väärtus


def cheated():
    global gospeed
    global gospeed_väärtus
    gospeed = 255
    gospeed_väärtus = 255
    meetrit_edasi(0.2)
    meetrit_tagasi(0.2)
    meetrit_edasi(0.2)
    meetrit_tagasi(0.2)
    meetrit_edasi(0.2)
    meetrit_tagasi(0.2)
    meetrit_edasi(0.2)
    meetrit_tagasi(0.2)
    gospeed_väärtus = 100
    gospeed = 100


# AUTONOMOUS
    
def autonomous():
    while True:
        
        ser.write("R".encode()) # Send something to the Arduino to indicate we're ready to get some data.
        serial_line = ser.readline().strip() # Read the sent data from serial.
        
        try:

            # Decode the received JSON data
            data = json.loads(serial_line.decode())
            # Extract the sensor value
            dist = data['us1']
            #dist2 = data['us2']
        except Exception as e:  # Something went wrong extracting the JSON.
            dist = -1# Handle the situation.
            print(e)
            pass


        if dist != -1:
            go.motor1(1,gospeed)
            go.motor2(1,gospeed)
            if dist < 900:
                go.right()

dist = -1

ser = serial.Serial('/dev/ttyUSB0', 9600)

# Make sure arduino is ready to send the data.
print("Syncing serial...0%\r", end='')
while ser.in_waiting == 0:
    ser.write("R".encode())
print("Syncing serial...50%\r", end='')
while ser.in_waiting > 0:
    ser.readline()
print("Syncing serial...100%")





