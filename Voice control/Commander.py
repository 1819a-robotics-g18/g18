import socket

class Commander:
    # IP, port
    def __init__(self, ip, port):
        self.ip = ip
        self.port = port
        self.client_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    def sendCommand(self, command):
        try:
            # Send the data
            self.client_socket.sendto(command.encode("utf8"), (self.ip, self.port))
        except error:
            # If an error occurs, print the error and close the socket
            print(error)
            self.closeSocket()

    # Closing the socket
    def closeSocket(self):
        self.client_socket.close()