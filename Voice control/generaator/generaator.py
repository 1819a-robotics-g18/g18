from gtts import gTTS
import os
#import vlc
import time
from pygame import mixer

# To use this module:
# from generaator.generaator import generate
# generate("My name is Jenny. I'm bored as hell!")

# Synthesize the voice and play back the input text
def generate(text):
    tts = gTTS(text)
    fail = text+".mp3"
    tts.save(fail)
    
    mixer.init()
    mixer.music.load(fail)
    mixer.music.play()
    
    #time.sleep(2)
    """
    p = vlc.MediaPlayer(fail)
    time.sleep(5)
    print("Edukas!")
    p.play()

    time.sleep(3)"""
    os.remove(fail)

def muusika(fail):
    mixer.init()
    mixer.music.load(fail)
    mixer.music.play()
    
def stopp():
    mixer.music.stop()
