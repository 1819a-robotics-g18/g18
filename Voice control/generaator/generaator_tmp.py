from gtts import gTTS
import os
import vlc
import tempfile

# Synthesize the voice and play back the input text
def generate(text):
    
    tts = gTTS(text)
    fail = text+".mp3"
    
    # Create a temporary file for playback
    with tempfile.NamedTemporaryFile(suffix='.mp3', delete=False) as f:
        tmpfile = f.name
    tts.save(tmpfile)
    
    # Playing the sound using VLC
    p = vlc.MediaPlayer(tmpfile)
    print("Edukas!")
    p.play()
    
    # Delete the tempfile
    os.remove(tmpfile)

    """os.system("cvlc -A alsa,none --alsa-audio-device default")
    p = vlc.MediaPlayer("file:///hello.mp3")
    print("Edukas!")
    p.play()"""
    
generate("tere")